package org.example.figuregenerator;

import org.example.figuregenerator.factory.FigureFactory;
import org.example.figuregenerator.figure.Figure;
import org.example.figuregenerator.figure.Square;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        FigureFactory figureFactory = new FigureFactory();
        List<Figure> figures = figureFactory.getFigures(10);

        for (var figure : figures) {
            System.out.println(figure.toString());
        }
    }
}
