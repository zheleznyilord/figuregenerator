package org.example.figuregenerator.figure;

public class Circle extends Figure{
    private int radius;

    public Circle(int radius,String color) {
        this.radius = radius;
        setArea(calculateArea());
        setColor(color);
    }

    @Override
    public void draw() {
        System.out.println(String.format("Drawing %s circle",getColor()));
    }

    @Override
    public int calculateArea() {
        return (int) (3.14*radius*radius);
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
        setArea(calculateArea());
    }

    @Override
    public String toString() {
        return String.format("Фигура: круг, площадь %s кв.ед., радиус %s ед., цвет: %s",
                getArea(),radius,getColor());
    }
}
