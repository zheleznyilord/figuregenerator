package org.example.figuregenerator.figure;

public class Trapezoid extends Figure {
    private int upSide;
    private int downSide;
    private int height;

    public Trapezoid(int upSide, int downSide, int height, String color) {
        this.upSide = upSide;
        this.downSide = downSide;
        this.height = height;
        setColor(color);
        setArea(calculateArea());
    }

    @Override
    public void draw() {
        System.out.println(String.format("Drawing %s trapezoid",getColor()));
    }

    @Override
    public int calculateArea() {
        return height*(upSide + downSide)/2;
    }

    public int getUpSide() {
        return upSide;
    }

    public void setUpSide(int upSide) {
        this.upSide = upSide;
    }

    public int getDownSide() {
        return downSide;
    }

    public void setDownSide(int downSide) {
        this.downSide = downSide;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return String.format("Фигура: трапеция, площадь %s кв.ед., высота %s ед., цвет: %s",
                getArea(),height,getColor());
    }
}
