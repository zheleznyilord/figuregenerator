package org.example.figuregenerator.figure;

public class Triangle extends Figure {
    private int hypotenuse;
    private int firstCatet;
    private int secondCatet;

    public Triangle(int hypotenuse, int firstCatet, int secondCatet, String color) {
        this.hypotenuse = hypotenuse;
        this.firstCatet = firstCatet;
        this.secondCatet = secondCatet;
        setArea(calculateArea());
        setColor(color);
    }

    @Override
    public void draw() {
        System.out.println(String.format("Drawing %s triangle",getColor()));
    }

    @Override
    public int calculateArea() {
        return (firstCatet*secondCatet)/2;
    }

    public int getHypotenuse() {
        return hypotenuse;
    }

    public void setHypotenuse(int hypotenuse) {
        this.hypotenuse = hypotenuse;
    }

    public int getFirstCatet() {
        return firstCatet;
    }

    public void setFirstCatet(int firstCatet) {
        this.firstCatet = firstCatet;
        setArea(calculateArea());
    }

    public int getSecondCatet() {
        return secondCatet;
    }

    public void setSecondCatet(int secondCatet) {
        this.secondCatet = secondCatet;
        setArea(calculateArea());
    }

    @Override
    public String toString() {
        return String.format("Фигура: треугольник, площадь %s кв.ед., гипотенуза %s ед., цвет: %s",
                getArea(),hypotenuse,getColor());
    }
}
