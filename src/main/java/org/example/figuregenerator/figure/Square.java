package org.example.figuregenerator.figure;

public class Square extends Figure{
    private int side;

    public Square(int side, String color){
        this.side = side;
        setColor(color);
        setArea(calculateArea());
    }

    @Override
    public void draw() {
        System.out.println(String.format("Drawing %s square",getColor()));
    }

    @Override
    public int calculateArea() {
        return side*side;
    }

    public int getSide() {
        return side;
    }

    public void setSide(int side) {
        this.side = side;
        setArea(calculateArea());
    }

    @Override
    public String toString() {
        return String.format("Фигура: квадрат, площадь %s кв.ед., сторона %s ед., цвет: %s",
                getArea(),side,getColor());
    }
}
