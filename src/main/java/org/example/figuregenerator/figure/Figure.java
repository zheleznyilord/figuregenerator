package org.example.figuregenerator.figure;

public abstract class Figure {
    private int area;
    private String color;

    public abstract void draw();
    public abstract int calculateArea();

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
