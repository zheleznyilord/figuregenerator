package org.example.figuregenerator.factory;

import org.example.figuregenerator.figure.*;

import java.util.ArrayList;
import java.util.List;

public class FigureFactory {

    public Figure getFigure() {
        double random = Math.random();
        int type = (int) (random * 4);
        switch (type) {
            case 0:
                return new Square((int) (Math.random() * 10), "yellow");
            case 1:
                int rnd = (int) (Math.random() * 10);
                return new Triangle(rnd, rnd / 2, rnd / 3, "blue");
            case 2:
                return new Circle((int) (Math.random() * 10), "yellow");
            case 3:
                int base = (int) (Math.random() * 10);
                return new Trapezoid(base / 2, base, base / 2, "green");
            default:
                return null;
        }
    }

    public List<Figure> getFigures(int maxExpectedElements) {
        List<Figure> figures = new ArrayList<>();
        double size = Math.random() * maxExpectedElements;
        for (int i = 0; i < size + 1; i++) {
            figures.add(getFigure());
        }
        return figures;
    }
}
