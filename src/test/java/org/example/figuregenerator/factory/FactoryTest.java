package org.example.figuregenerator.factory;

import org.example.figuregenerator.figure.Figure;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class FactoryTest {
    FigureFactory factory = new FigureFactory();

    @Test
    public void TestGetFigure() {
        Figure figure = factory.getFigure();

        Assert.assertNotNull(figure);
    }

    @Test
    public void TestGetFigures() {
        List<Figure> figures = factory.getFigures(10);

        Assert.assertNotEquals(0,figures.size());
    }
}
