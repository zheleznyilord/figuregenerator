package org.example.figuregenerator.figure;

import org.junit.Assert;
import org.junit.Test;

public class FigureTest {

    @Test
    public void TestCircleArea() {
        Circle circle = new Circle(5, "yellow");
        int expected = (int) (3.14 * 5 * 5);

        Assert.assertEquals(expected, circle.calculateArea());
    }

    @Test
    public void TestSquareArea() {
        Square square = new Square(6, "red");
        int expected = 6 * 6;

        Assert.assertEquals(expected, square.calculateArea());
    }

    @Test
    public void TestTrapezoidArea() {
        Trapezoid trapezoid = new Trapezoid(5, 11, 6, "white");
        int expected = 6 * (5 + 11) / 2;

        Assert.assertEquals(expected, trapezoid.calculateArea());
    }

    @Test
    public void TestTriangleArea() {
        Triangle triangle = new Triangle(10, 5, 6, "blue");
        int expected = 5 * 6 / 2;

        Assert.assertEquals(expected, triangle.calculateArea());
    }

}
